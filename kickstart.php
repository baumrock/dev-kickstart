<?php
/**
 * ProcessWire Kickstart Installation
 * 
 * @author Bernhard Baumrock, baumrock.com
 * MIT Licence
 */
class Kickstart {

  /**
   * variables
   */
  public $step; // current installation step
  public $next; // next installation step
  public $h; // headline
  public $content; // main markup
  private $alerts = []; // array of all alert messages
  private $numerr = 0; // number of errors
  private $host; // host including domain
  private $ks; // kickstart config
  private $root; // site root path
  public $defaults; // default config variables
  private $wire; // processwire instance after install
  
  /**
   * init kickstart class
   */
  public function __construct() {
    ini_set('max_execution_time', 60*5);

    // load tracy debugger if present
    $this->tracy();

    // set variables
    $this->step = isset($_POST['step']) ? (string)$_POST['step'] : 'welcome';
    $this->next = 'welcome'; // if no next step is defined we fallback to welcome
    $this->host = $_SERVER['HTTP_HOST'];
    $this->root = getcwd();

    // set defaults
    $host = $this->host;
    $dbname = strtok($host, '.');
    $this->defaults = [
      // site config
      'chmodDir' => 755,
      'chmodFile' => 644,
      'timezone' => null,
      'httpHosts' => "www.$host\n$host",
      'dbUser' => null,
      'dbName' => $dbname,
      'dbPass' => null,
      'dbHost' => ini_get("mysqli.default_host") ?: 'localhost',
      'dbPort' => ini_get("mysqli.default_port") ?: 3306,
      'dbUser' => ini_get("mysqli.default_user"),
      'dbPass' => ini_get("mysqli.default_pw"),
      'dbEngine' => 'MyISAM',
      'dbCharset' => 'utf8',

      // admin login
      'admin_name' => 'admin',
      'username' => 'admin',
      'userpass' => 'admin1!',
      'userpass_confirm' => 'admin1!',
      'useremail' => null,
      'remove_items' => [
        'install-php',
        'install-dir',
        'gitignore',
        'site-beginner',
        'site-languages',
        'site-blank',
        'site-classic',
        'site-default',
      ],
    ];
  }
  
  /**
   * add msg/success/error messages to msg array
   */
  private function msg($str) { $this->alerts[] = ['msg', $str]; }
  private function err($str) { $this->alerts[] = ['err', $str]; $this->numerr++; }
  private function warn($str) { $this->alerts[] = ['warn', $str]; }
  private function succ($str) { $this->alerts[] = ['succ', $str]; }
  
  /**
   * parse returned html from pw installer
   */
  public function parse($html) {
    libxml_use_internal_errors(true);
    $doc = new DOMDocument();
    $doc->loadHTML($html);
    
    // loop through all list items
    foreach($doc->getElementsByTagName('li') as $li) {
      $outer = $doc->saveHTML($li);
      $inner = trim($li->nodeValue);

      if(strpos($outer, '<li class="ui-state-highlight">') === 0) bd("success: $inner");
      elseif(strpos($outer, '<li class="ui-state-error ui-priority-secondary">') === 0) {
        $this->warn($inner);
        bd("warning: $inner");
      }
      elseif(strpos($outer, '<li class="ui-state-error">') === 0) {
        $this->numerr++;
        $this->err($inner);
        bd("error: $inner");
      }
    }

    libxml_clear_errors();

    // return the domdocument if any further actions are required
    return $doc;
  }

  /**
   * check compatibility
   */
  private function checkCompatibility() {
    bd('checkCompatibility');

    // if installer does not exist return
    if(!is_file('install.php')) {
      $this->err('No install.php found - unable to run compatibility check!');
      return;
    }

    // pw-installer checks
    $this->parse($this->post([
      'step' => 1,
    ]));

    return $this->numerr ? false : true;
  }

  /**
   * if code was submitted update kickstartfile
   */
  private function updateKickstartfile() {
    $code = @$_POST['kickstartfile'];
    if(!$code) return;
    else file_put_contents('kickstartfile.php', $code);
  }
  
  /**
   * is a pw installation available?
   */
  private function findPW() {
    // dont check for site folder because we create it at compatibilitycheck
    return (is_file('install.php') OR is_dir('wire'));
  }

  /**
   * check php code
   */
  private function checkPHP($file) {
    exec("php -l $file", $out);
    if(strpos($out[0], 'No syntax errors detected in') === 0) return true;
    else {
      $this->err(implode("<br>", array_filter($out)));
      return false;
    }
  }

  /**
   * get kickstartfile
   */
  private function loadKickstartToConfig() {
    bd('loadKickstartToConfig');

    if(!is_file('kickstartfile.php')) {
      if($this->step != 'welcome') $this->err('Unable to find Kickstartfile');
      return;
    }

    // read installation config from kickstartfile
    // throw error if we find a parse error
    if($this->checkPHP('kickstartfile.php')) {
      $str = file_get_contents('kickstartfile.php');      
      if(strpos($str, '<?php // kickstart') !== 0) {
        $this->err('The Kickstartfile has to begin exactly with "&lt;?php // kickstart"');
        unlink('kickstartfile.php');
        @$_POST['kickurl'] = null;
        return;
      }

      // include the kickstartfile
      $ks = include('kickstartfile.php');

      // check if config is an array
      if(!is_array($ks)) $this->err('Your kickstartfile has to return an array!');

      // check if pw is present or a downloadurl is set
      $pwfile = $this->findPW();
      $pwurl = @$ks['config']['url'];
      if(!$pwfile AND !$pwurl) $this->err('PW must either be available in siteroot or a download-url has to be specified');
      elseif($pwfile AND $pwurl) $this->err('PW found in siteroot AND download url specified. Remove one of those two options to continue');
    }

    // save config
    $this->ks = $ks;
    return $this->numerr ? false : true;
  }

  /**
   * download zip, excract content and delete zipfile
   */
  private function downloadAndExtractTo($url, $todir) {
    // download pw zip
    $zipfile = $this->downloadFile($url, 'tmp.zip');
    
    // extract zip and save foldername
    $zipdir = $this->extractZip($zipfile);

    // move extracted files to root folder
    $this->recursiveMove($this->root . "/" . $zipdir, $todir);
    
    // remove extracted zip directory
    $this->removeDir($this->root . "/" . $zipdir, true);

    // we remove also zip file
    unlink($zipfile);
  }

  /**
   * install module from recipe
   */
  private function installModule($modulename, $url = null) {
    if($url) {
      if(is_dir($dir = "../site/modules/$modulename")) $this->warn("Module $modulename already exists");
      else $this->downloadAndExtractTo($url, $dir);
    }
    $this->wire->modules->resetCache();
    $this->wire->modules->get($modulename);
  }

  /**
   * activate site profile
   */
  private function siteProfile() {
    bd('siteProfile');

    $profile = @$this->ks['config']['profile'];
    if(!$profile) {
      $this->err('No site profile found!');
      return;
    }

    // create tmp dir name and remove site folder
    $tmpdir = $this->root . '/site-' . md5($profile);
    $this->removeDir('site', true);

    // check type of profilestring
    switch($this->linkType($profile)) {
      // download the given profile
      case 'url':
        $this->downloadAndExtractTo($profile, $tmpdir);
        return rename($tmpdir, 'site');
        break;

      // just rename the folder
      case 'dir':
        return rename($profile, 'site');
        break;

      // extract & rename
      case 'zip':
        $succ = rename($this->extractZip($profile), 'site');
        @unlink($profile);
        return $succ;
        break;
      
      default:
        $this->err('The specified site profile is invalid!');
        return;
    }

    return true;
  }

  /**
   * grab pw if it is not present
   */
  private function grabPW() {
    bd('grabPW');
    
    if($this->findPW()) return true;
    
    $url = @$this->ks['config']['url'];
    if(!$url) {
      $this->err('No download-url for PW specified');
      return;
    }
    else $this->downloadAndExtractTo($url, $this->root);
    
    return true;
  }

  /**
   * get link type of given config setting
   * options can be:
   * 
   * url: the given string is a web-address
   * dir: the given name is a folder on the local server
   * zip: the given name is a zip file on the local server
   */
  private function linkType($str) {
    if(strpos($str, 'http://') === 0 OR strpos($str, 'https://') === 0) return 'url';
    elseif(is_dir($str)) return 'dir';
    elseif(is_file($str)) {
      $ext = @pathinfo($str)['extension'];
      return $ext;
    }
    else return false;
  }

  /**
   * send data to pw installer
   */
  private function sendToPW($data) {
    bd('sendToPW');

    $data = array_merge($this->defaults, $data);
    
    // merge settings from kickstartfile
    if(is_array(@$this->ks['config'])) $data = array_merge($data, $this->ks['config']);

    // send data to pw installer
    $this->parse($this->post($data));

    return $this->numerr ? false : true;
  }

  /**
   * execute recipes
   */
  private function recipes() {
    if(!@is_array($this->ks['recipes'])) return;
    if(!is_dir('recipes')) mkdir('recipes');

    // bootstrap pw
    include('index.php');
    $this->wire = $wire;

    // init pw by visiting the admin url
    $this->get($wire->pages->get(2)->httpUrl);

    // loop all recipes
    chdir('recipes');
    foreach($this->ks['recipes'] as $recipe) {
      bd($recipe);
      switch($this->linkType($recipe)) {
        case 'url':
          $this->include($this->downloadFile($recipe, md5($recipe).'.php'));
          break;

        case 'php':
          $this->include($recipe);
          break;

        default:
          $this->warn("Recipe $recipe not found or not valid");
          break;
      }
    }
    chdir('../');

    return $this->numerr ? false : true;
  }

  /**
   * include file
   */
  private function include($file) {
    // do some checks
    if(!is_file($file)) return;
    $str = file_get_contents($file);
    if(strpos($str, '<?') !== 0) return;

    // if pw is already loaded we assign the $wire variable
    if($this->wire) $wire = $this->wire;

    // include file
    include($file);
  }

  /**
   * cleanup kickstart
   */
  private function cleanup() {
    bd('cleanup');

    /*
    $this->removeDir('vendor');
    unlink('kickstart.php');
    unlink('kickstartfile.php');
    $this->removeDir('recipes');
    */
  }

  /**
   * execute this kickstart
   */
  public function execute() {
    // handle special requests
    if(isset($_GET['action'])) {
      switch($_GET['action']) {

        // this is to handle file uploads of kickstartfile
        case 'upload':
          $upload = @move_uploaded_file($_FILES["files"]["tmp_name"][0], 'kickstartfile.php');
          echo json_encode(['upload'=>$upload]);
          die();
          break;

        // by default if an action is set via GET we execute this action as step
        default:
          $this->step = $_GET['action'];
      }
    }

    // execute the given screen
    if(is_callable(array($this, $this->step))) $this->{$this->step}();
    else $this->welcome();
  }

  /******************* screens *******************/

  /**
   * welcome screen
   */
  private function welcome() {
    $this->step = 'welcome';
    $this->next = 'checkKickstartfile';
    $this->h = 'Welcome to PW Kickstart!';
    
    // if we find a valid kickstart config we proceed to the next step
    if($this->loadKickstartToConfig()) {
      $this->checkKickstartfile();
      return;
    }

    ob_start();
    ?>
    <div class="uk-card uk-card-default uk-card-body uk-margin-bottom">
      <h4>Recipe Editor</h4>
      <?= $this->b([
        'label' => 'Proceed to the Recipe Editor',
        'name' => 'recipeEditor',
      ]); ?>
    </div>

    <div class="uk-card uk-card-default uk-card-body uk-margin-bottom">
      <h4>Upload Kickstartfile</h4>
    
      <div class="js-upload uk-placeholder uk-text-center">
        <span uk-icon="icon: cloud-upload"></span>
        <span class="uk-text-middle">Drop your Kickstartfile here</span>
        <div uk-form-custom>
          <input type="file" multiple>
          <span class="uk-link">or select it by clicking here</span>
        </div>
      </div>
      <progress id="js-progressbar" class="uk-progress" value="0" max="100" hidden></progress>

    </div>

    <div class="uk-card uk-card-default uk-card-body">
      <h4>Load a Kickstartfile from URL</h4>
      <div class="uk-inline uk-width-1-1">
        <span class="uk-form-icon" uk-icon="icon: github"></span>
        <input class="uk-input" name="kickurl">
      </div>
      <p>Example Snippet:
        <span class="uk-margin-left"><a href="https://gitlab.com/baumrock/dev-kickstart/raw/master/sample-kickstartfile.php" class="copy"><i class="fa fa-files-o"></i> Copy Link to Input</a></span>
        <span class="uk-margin-left"><a href="https://gitlab.com/baumrock/dev-kickstart/raw/master/sample-kickstartfile.php" target="_blank"><i class="fa fa-external-link"></i> Open File in Browser</a></span>
      </p>
    </div>

    <script>
      var bar = document.getElementById('js-progressbar');
      UIkit.upload('.js-upload', {
        url: './kickstart.php?action=upload',
        multiple: false,
        error: function () {
          $('.uploaderror').removeClass('uk-hidden');
        },
        complete: function () {
          $('button[type=submit]').click();
        },
        loadStart: function (e) {
          bar.removeAttribute('hidden');
          bar.max = e.total;
          bar.value = e.loaded;
        },
        progress: function (e) { bar.max = e.total; bar.value = e.loaded; },
        loadEnd: function (e) { bar.max = e.total; bar.value = e.loaded; },
      });

      // copy link to input
      $('a.copy').on('click', function() {
        $('input[name=kickurl]').val($(this).attr('href'));
        return false;
      });

      // handle clicks on recipeEditor
      $('button[name=recipeEditor]').click(function() {
        // change the next step
        $('input[name=step]').val('recipeEditor');
      });
    </script>
    <?php
    echo $this->b([
      'label' => 'Next: Check Kickstartfile >',
      'class' => 'uk-margin-medium-top uk-button-primary',
    ]);
    $this->content = ob_get_clean();
  }

  /**
   * kickstartfile content screen
   */
  private function checkKickstartfile() {
    if($this->step == 'welcome') $this->succ('We found a kickstartfile in the root folder, so we moved on directly to this step!');

    $this->step = 'checkKickstartfile';
    $this->next = 'install';
    $this->h = 'Please check your Kickstartfile';

    // if url was specified download file
    if(@$_POST['kickurl']) $this->downloadFile($_POST['kickurl'], 'kickstartfile.php');
    // otherwise update kickstartfile (if code was provided)
    else $this->updateKickstartfile();

    // load kickstart to config to show errors if invalid
    $this->loadKickstartToConfig();

    ob_start();
    ?>
    
    <div class="uk-card uk-card-default uk-card-body">

      <textarea id="editor"><?= @file_get_contents('kickstartfile.php'); ?></textarea>
      <textarea name="kickstartfile" class="uk-hidden"></textarea>

      <?= $this->b([
        'label' => 'Check again',
        'name' => 'checkagain',
        'class' => 'uk-button-secondary',
      ]); ?>
      <?php if(!$this->numerr) echo $this->b([
        'label' => 'Install',
        'name' => 'install',
        'class' => 'uk-button-primary',
      ]); ?>

      <p><strong>Defaults</strong></p>
      <?php d($this->defaults) ?>

    </div>


    <script>
      $(document).ready(function() {
        // int ace editor
        var editor = ace.edit("editor");
        editor.getSession().setMode("ace/mode/php");
        editor.setOptions({
          maxLines: Infinity,
          tabSize: 2,
        });
        editor.focus(); //To focus the ace editor
        var n = editor.getSession().getValue().split("\n").length; // To count total no. of lines
        editor.gotoLine(n); //Go to end of document

        // hide install button when code was changed
        editor.getSession().on("change", function () {
          $('button[name=install]').hide();
        });

        // handle check again button-clicks
        $('button[name=checkagain]').click(function() {
          // change the next step
          $('input[name=step]').val('checkKickstartfile');
        });

        // update textarea value on form submit
        $('#form').on('submit', function() {
          $('textarea[name=kickstartfile]').val(editor.getSession().getValue());
        });
      });
    </script>
    <?php
    $this->content = ob_get_clean();
  }
  
  /**
   * recipe editor
   */
  private function recipeEditor() {
    $this->step = 'recipeEditor';
    $this->next = 'recipeEditor';
    $this->h = 'Recipe Editor';

    // get selected file
    $recipes = array_diff(scandir('recipes'), array('..', '.'));
    sort($recipes); // reset the array keys
    $file = @$_POST['file'];
    if(!in_array($file, $recipes)) $file = $recipes[0];
    $filename = __DIR__ . '/recipes/'.$file;

    // if the run button was pressed we save the changes to the file
    if(@$_POST['run']) file_put_contents($filename, $_POST['code']);

    ob_start();
    ?>
    <p>To create a new recipe simply place a new php-file in the /recipes folder</p>
    
    <div class="uk-card uk-card-default uk-card-body">
      <select name="file" class="uk-select">
        <?php
        foreach($recipes as $recipe) {
          $selected = $recipe==$file ? ' selected' : '';
          echo "<option value='$recipe'$selected>$recipe</option>";
        }
        ?>
      </select>

      <textarea id="editor"><?= file_get_contents($filename); ?></textarea>
      <textarea name="code" class="uk-hidden"></textarea>

      <?= $this->b([
        'label' => 'Run (CTRL+ENTER)',
        'name' => 'run',
        'class' => 'uk-button-primary',
      ]); ?>

      <?php
      // run recipe
      if($this->checkPHP($filename) AND @$_POST['run']) {
        if(!is_file('index.php')) $this->err('index.php not found - execution of recipe not possible');
        else {
          echo '<p><strong>Output</strong></p>';
          include('index.php');
          $this->wire = $wire;
          $this->include($filename);
        }
      }
      ?>
    </div>

    <script>
      $(document).ready(function() {
        // int ace editor
        var editor = ace.edit("editor");
        editor.getSession().setMode("ace/mode/php");
        editor.setOptions({
          maxLines: Infinity,
          tabSize: 2,
        });
        editor.focus(); //To focus the ace editor
        var n = editor.getSession().getValue().split("\n").length; // To count total no. of lines
        editor.gotoLine(n); //Go to end of document

        // update textarea value on form submit
        $('#form').on('submit', function() {
          $('textarea[name=code]').val(editor.getSession().getValue());
        });

        // file select changed
        $('select[name=file]').change(function() {
          $('#form').submit();
        });

        // submit form on ctrl+enter
        $('#form').keydown(function (e) {
          if (e.ctrlKey && e.keyCode == 13) {
            $('button[name=run]').click();
          }
        });
      });
    </script>
    <?php
    $this->content = ob_get_clean();
  }
  
  /**
   * install processwire
   */
  private function install() {
    $this->step = 'install';
    $this->next = 'finish';
    $this->h = 'Installation Summary';

    if($this->loadKickstartToConfig())
      if($this->grabPW())
        if($this->siteProfile())
          if($this->checkCompatibility())
            if($this->sendToPW(['step' => 4]))
              if($this->sendToPW([ 'step' => 5 ]))
                if($this->recipes())
                  $this->cleanup();

    if($this->numerr) {
      $this->err('Installation failed!');
      return;
    }


    $this->succ('Installation successful!');
    $data = array_merge($this->defaults, $this->ks['config']);
    ob_start();
    ?>
    <p>
      <strong>URL to Admin:</strong> <a href="<?= $data['admin_name'] ?>"><?= $data['admin_name'] ?></a><br>
      <strong>Admin Username:</strong> <?= $data['username'] ?><br>
      <strong>Admin Password:</strong> <?= $data['userpass'] ?>
    </p>
    <?php
    $this->content = ob_get_clean();
  }

  /******************* output *******************/
  
  /**
   * render error messages
   */
  public function renderAlerts() {
    $out = '';
    foreach($this->alerts as $alert) {
      switch($alert[0]) {
        case 'msg': $cls = ''; $icon = 'info'; break;
        case 'err': $cls = 'uk-alert-danger'; $icon = 'bell'; break;
        case 'succ': $cls = 'uk-alert-success'; $icon = 'check'; break;
        case 'warn': $cls = 'uk-alert-warning'; $icon = 'warning'; break;
      }
      $out .= "<div class='$cls' uk-alert><a class='uk-alert-close' uk-close></a><p><span class='uk-margin-right' uk-icon='icon: $icon'></span> {$alert[1]}</p></div>";
    }
    return $out ? '<div class="uk-card uk-card-default uk-card-body">'.$out.'</div>' : '';
  }

  /**
   * render headline
   */
  public function renderHeadline() {
    if(!$this->h) return;
    return '<h1 class="uk-text-primary">' . $this->h . '</h1>';
  }

  /**
   * render content
   */
  public function renderContent() {
    return $this->content;
  }
  
  /**
   * render button
   */
  public function b($opt = []) {
    $defaults = [
      'label' => 'Next',
      'name' => uniqid(),
      'class' => 'uk-button-primary',
    ];
    $opt = array_merge($defaults, $opt);

    return "<button type='submit' value='1' name='{$opt['name']}' class='uk-button {$opt['class']}'>{$opt['label']}</button>";
  }

  /******************* somas online installer *******************/
  
  /**
   * Download file form a url
   * @param  string $url      source url
   * @param  string $fileName target file
   * @return [type]           [description]
   */
  private function downloadFile($url, $fileName) {
    if(!$url) return;

    if((substr($url,0,8) == 'https://') && ! extension_loaded('openssl')) {
      $this->err('OpenSSL extension required but not available. File could not be downloaded from '.$url);
      return;
    }
  
    // Define the options
    $options = array('max_redirects' => 4);
    $context = stream_context_create(array('http' => $options));
  
    // download the zip
    if(!$content = file_get_contents($url, $fileName, $context)) {
      $this->err('File could not be downloaded: '.$url);
      return;
    }
  
    if(($fp = fopen($fileName, 'wb')) === false) {
      $this->err('fopen error for filename '.$fileName);
      return;
    }
  
    fwrite($fp, $content);
    fclose($fp);

    return $fileName;
  }
  
  /**
   * extract zipname
   */
  private function extractZip($zipname) {
    $zip = new ZipArchive;
    if ($zip->open($zipname) === TRUE) {
      for($i = 0; $i < $zip->numFiles; $i++) {
        $zip->extractTo($this->root, array($zip->getNameIndex($i)));
      }
      $extracted_directory = $zip->getNameIndex(0);
      $zip->close();
    } else {
      die('Error opening downloaded ' . $zipname);
    }
    return $extracted_directory;
  }
  
  /**
   * recursively remove directory and it's content, this works great even where unlink() fails
   * @param  string $dir      folder to remove files
   * @param  boolean $DeleteMe should folder be remove also set this to true
   */
  private function removeDir($dir, $deleteDir) {
    if(!$dh = @opendir($dir)) return;
    while (($obj = readdir($dh))) {
      if($obj=='.' || $obj=='..') continue;
      if (!@unlink($dir.'/'.$obj)) $this->removeDir($dir.'/'.$obj, true);
    }
    if ($deleteDir){
      closedir($dh);
      @rmdir($dir);
    }
  }
  
  /**
   * Recursively move files from one directory to another
   *
   * @param String $src - Source of files being moved
   * @param String $dest - Destination of files being moved
   */
  private function recursiveMove($src, $dest){
  
    // If source is not a directory stop processing
    if(!is_dir($src)) return false;
  
    // If the destination directory does not exist create it
    if(!is_dir($dest)) {
      if(!mkdir($dest)) {
        // If the destination directory could not be created stop processing
        return false;
      }
    }
  
    // Open the source directory to read in files
    $i = new DirectoryIterator($src);
    foreach($i as $f) {
      if($f->isFile()) {
        rename($f->getRealPath(), "$dest/" . $f->getFilename());
      } else if(!$f->isDot() && $f->isDir()) {
        $this->recursiveMove($f->getRealPath(), "$dest/$f");
        //unlink($f->getRealPath());
      }
    }
  }

  /******************* helper functions *******************/
  
  /**
   * send data to pw-installer via curl post
   */
  public function post($data, $url = null) {
    if(!function_exists('curl_version')) {
      $this->err('CURL is required for PW Kickstart');
      return;
    }

    if(!$url) $url = $this->host.'/install.php';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $out = curl_exec($ch);
    curl_close ($ch);
    return $out;
  }
  
  /**
   * curl get request
   */
  public function get($url = null) {
    if(!function_exists('curl_version')) {
      $this->err('CURL is required for PW Kickstart');
      return;
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $out = curl_exec($ch);
    curl_close ($ch);
    return $out;
  }

  /**
   * return random password
   */
  public function randomPassword($len = 8) {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!§$%&/()=?';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $len; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
  }

  /**
   * load tracy if present
   */
  private function tracy() {
    if(!is_file('tracy/tracy.php')) {
      function bd($data) {}
      function d($data) {}
      return;
    }
    require_once('tracy/tracy.php');
    \Tracy\Debugger::enable();
    function bd($data) { \Tracy\Debugger::barDump($data); }
    function d($data) { \Tracy\Debugger::dump($data); }
  }
}

// init kickstart class
$kick = new Kickstart();
$kick->execute();

?><!DOCTYPE html>
<html>
  <head>
    <title>PW Kickstart powered by baumrock.com</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://www.baumrock.com/site/templates/favicon/favicon.ico">

    <!-- CSS -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.31/css/uikit.min.css" />

    <!-- JS -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.31/js/uikit.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.31/js/uikit-icons.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/zepto/1.2.0/zepto.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ace/1.2.9/ace.js"></script>

    <!-- custom styles -->
    <style>
    a:hover { text-decoration: none; }
    </style>
  </head>
  <body class="uk-background-muted step-<?= $kick->step ?>">
  
    <section class="uk-section-primary">
      <div class="uk-container uk-text-center uk-padding-small">
        <a href="./kickstart.php" style="text-decoration: none;"><span uk-icon="icon: bolt"></span> ProcessWire Kickstart</a>
      </div>
    </section>
  
    <section class="uk-container uk-padding" id="main" uk-height-viewport="expand: true">
      <form action="./kickstart.php" method="post" id="form">
        <?= $kick->renderAlerts() ?>
        <?= $kick->renderHeadline() ?>
        <?= $kick->renderContent() ?>
        <input type="hidden" name="step" value="<?= $kick->next ?>">
      </form>
    </section>

    <section class="uk-background-secondary">
      <div class="uk-container uk-text-center uk-padding-small">
        <p>powered by <a href="https://www.baumrock.com">baumrock.com</a></p>
      </div>
    </section>
    
  </body>
</html>