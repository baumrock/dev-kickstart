<?php // kickstart
// the first line has to begin exactly with <?php // kickstart
// you can use all the methods from the kickstart installer via the $this variable, eg $this->randomPassword()
return [
  
  // config data related to the processwire installation
  'config' => [
    
    // url to processwire installation zip-file
    'url' => 'https://github.com/processwire/processwire/archive/93d1be845358bd143899bae5698751a555db96a5.zip',
    
    // which profile should be installed?
    //'profile' => 'site-default', // it can be a folder name
    //'profile' => 'site-blank.zip', // or a zip file
    //'profile' => 'https://gitlab.com/baumrock/kickstart/raw/master/sample-profile.zip', // or an url to a zip file
    //'profile' => 'https://transfer.sh/Ki7td/site-blank.zip',
    'profile' => 'site-blank',

    // installation settings
    // here you can overwrite the default settings of file kickstart.php
    'timezone' => 367, // Europe/Vienna
    'dbUser' => 'root',
    //'dbPass' => $this->randomPassword(),
  ],
  
  // recipes of what to do after installation
  // the current pw installation is available as $wire
  'recipes' => [
    'sample-recipe.php',
    //'https://gitlab.com/snippets/1681610/raw',
  ],
];
