<?php
$this->msg('Sample Recipe to install AdminThemeReno and assign it to guest and admin');

$this->installModule('AdminThemeReno');
$user = $wire->users->get(40)->setAndSave('admin_theme', 'AdminThemeReno');
$user = $wire->users->get(41)->setAndSave('admin_theme', 'AdminThemeReno');

$this->msg('Now visit your backend - you will see that Reno is installed and set');

$this->installModule('TracyDebugger', 'https://github.com/adrianbj/TracyDebugger/archive/master.zip');

$this->msg('Tracy installed');
